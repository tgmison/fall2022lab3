//Tyler Mison
//1511539
package linearalgebra;
import static org.junit.Assert.*;

import org.junit.Test;


public class Vector3dTests {
    
    @Test
    public void getterTester(){
        Vector3d[] getTest = new Vector3d[1];

        getTest[0] = new Vector3d(1.0, 1.0, 2.0);
        
        double testX = 1.0;
        double testY = 1.0;
        double testZ = 2.0;
        
        assertTrue(testX==getTest[0].getX());
        assertTrue(testY==getTest[0].getY());
        assertTrue(testZ==getTest[0].getZ());
    }

    @Test
    public void magTester(){
        Vector3d[] getTest = new Vector3d[1];

        getTest[0] = new Vector3d(1.0, 1.0, 2.0);

        double magAns = 2.449489742783178;

        assertTrue(magAns==getTest[0].magnitude());
    }

    @Test
    public void dotTester(){
        Vector3d[] getTest = new Vector3d[2];

        getTest[0] = new Vector3d(1.0, 1.0, 2.0);
        getTest[1] = new Vector3d(2.0, 3.0, 4.0);

        double dotAnswer=13.0;
        assertTrue(dotAnswer==(getTest[0].dotProduct()+getTest[1].dotProduct()));
    }

    @Test
    public void addTester(){

    }


}

//Tyler Mison
//Student ID: 1511539

/**
 * 
 * 
 * 
 * 
 * 
 * 
 */
public class Vector3d {
    
    //3 fields
    private double x;
    private double y;
    private double z;

    //Constructor
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Get Method for X
    public double getX() {
        return x;
    }

    //Get Method for Y
    public double getY() {
        return y;
    }

    //Get Method for Z
    public double getZ() {
        return z;
    }

    //magnitude Method
    public void magnitude(){
        
    }
}

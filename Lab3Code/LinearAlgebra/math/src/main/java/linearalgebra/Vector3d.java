//Tyler Mison
//1511539
package linearalgebra;
import java.lang.Math;
import java.io.*;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;


    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return x;
    }


    public double getY() {
        return y;
    }


    public double getZ() {
        return z;
    }

    public double magnitude(){
        return Math.sqrt((x * x) + (y * y) + (z * z));
    }

    public double dotProduct(){
        return (((x + this.x) + (y + this.y) + (z + this.z))/2);

    }

    public Vector3d add() {
        return new Vector3d((this.x + x),(this.y + y),(this.z + z));
    }
    public static void main(String[] args){
        Vector3d[] getTest = new Vector3d[2];

        getTest[0] = new Vector3d(1.0, 1.0, 2.0);

        getTest[1] = new Vector3d(2.0, 3.0, 4.0);
        
        System.out.println(Math.sqrt(6));
        System.out.println(getTest[1].dotProduct()+getTest[0].dotProduct());
        

        
    }
}
